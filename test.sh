#!/bin/bash

file=tw.txt
host=(
api.ipify.org
api.gamer.com.tw
gamer2-cds.cdn.hinet.net
)
dns=(
1.1.1.1
8.8.8.8
)

for h in ${host[@]}; do
  for d in ${dns[@]}; do
    nslookup $h $d | egrep -o '[0-9]{1,3}(\.[0-9]{1,3}){3}'|egrep -v $d >> $file
  done
done
sort $file|uniq > $file2.txt
mv -f $file2.txt $file
#while IFS= read -r line; do
    #echo "ip=$line"
#done < /sdcard/tw.txt
